"""
transform pair of indeces in compressed format number_of_repetitions * value
to linear sequence [value] sizeof number_of_repetitions
"""
def index_pairs_to_vec(index_pairs):
    try:
        splitted = index_pairs.split(",")
        vec = []
        for index_pair in splitted:
            pair_vec = index_pair_to_vec(index_pair)
            if(pair_vec is None):
                raise Exception("Compressed vector representation parsing error")
            else:
                vec = vec + pair_vec
        return vec
    except Exception:
        return None


"""
transform pair of indeces in compressed format number_of_repetitions * value
to linear sequence [value] sizeof number_of_repetitions
"""
def index_pair_to_vec(index_pair):
    try:
        splitted = index_pair.split("*")
        rep = int(splitted[0])
        val = int(splitted[1])
        vec = []
        for i in range(rep):
            vec.append(val)
        return vec
    except Exception:
        return None

print(index_pairs_to_vec("3*1,5*2,2*3"))