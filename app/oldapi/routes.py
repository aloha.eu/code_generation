# -*- coding: utf-8 -*-

###
### DO NOT CHANGE THIS FILE
### 
### The code is auto generated, your change will be overwritten by 
### code generating.
###
from __future__ import absolute_import

from .api.eval_perf_status import EvalPerfStatus
from .api.power_performance_evaluations import PowerPerformanceEvaluations


routes = [
    dict(resource=EvalPerfStatus, urls=['/eval_perf/status'], endpoint='eval_perf_status'),
    dict(resource=PowerPerformanceEvaluations, urls=['/power_performance_evaluations'], endpoint='power_performance_evaluations'),
]